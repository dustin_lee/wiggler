import math as ma
import numpy as np
from scipy.special import kv as mod_bessel

import matplotlib.pyplot as plt

from parameters import *


# Get the data.
energies = np.load("data/energies.npy")


# Compute the brilliances.
central_energies = np.linspace(1., 500., 1000)
simulated_brilliances = np.zeros(central_energies.shape)
predicted_brilliances = np.zeros(central_energies.shape)
for i, central_energy in enumerate(central_energies):
    # Compute the predicted brilliance.
    r = central_energy/(4.17 * electron_energy_GeV**2. * max_magnetic_field)
    brilliance = (1.33e13
                  * num_periods
                  * electron_energy_GeV**2.
                  * current
                  * r**2.
                  * mod_bessel(3./2., r/2.))
    predicted_brilliances[i] = brilliance

    # Compute the simulated brilliance
    # Get the number of photons within %0.1BW.
    energy_width = .001*central_energy
    lower_idx = np.searchsorted(energies,
                                central_energy-energy_width/2.,
                                side="left")
    upper_idx = np.searchsorted(energies,
                                central_energy+energy_width/2.,
                                side="right")
    count = upper_idx - lower_idx

    # Weigh the count by the current.
    count *= (current*(1./1.602176634e-19)/num_electrons)
    # Compute the brilliance.
    brilliance = count/(vertical_opening_angle*1000.
                        * horizontal_opening_angle*1000.
                        * beam_cross_section)
    simulated_brilliances[i] = brilliance

# Plot the data.
plt.plot(central_energies, predicted_brilliances, label="predicted")
plt.plot(central_energies, simulated_brilliances, label="simulated")
plt.xlim(1.)
plt.ylim(0.)
plt.xlabel("energy (keV)")
plt.ylabel("brilliance ((count/s)/(mm^2⋅mrad^2))")
plt.legend()
plt.show()
