import csv
import numpy as np

from parameters import *


# Get the raw data.
energies = []
locations = []
x_coords = []
y_coords = []
with open("data/data.csv") as temp:
    reader = csv.reader(temp)
    for row in reader:
        energy = float(row[0])
        if energy > 1.:
            x = float(row[1])
            y = float(row[2])
            energies.append(energy)
            x_coords.append(x)
            y_coords.append(y)
            locations.append(np.array([x, y]))
print("The raw data was read.")

# Get the intensity.
num_bins_per_side = 100
bins = np.linspace(-detector_radius, detector_radius, num_bins_per_side+1)
X, Y = np.meshgrid(bins, bins)
x_indices = np.digitize(x_coords, bins)
y_indices = np.digitize(y_coords, bins)
total_energies = np.zeros((num_bins_per_side, num_bins_per_side))
for x_idx, y_idx, energy in zip(x_indices, y_indices, energies):
    if not (x_idx <= 0
            or y_idx <= 0
            or x_idx > num_bins_per_side
            or y_idx > num_bins_per_side):
        total_energies[x_idx-1, y_idx-1] += energy
# Weight by the current and the bin area.
total_energies *= (current*(1./1.602176634e-19)/num_electrons)
bin_len = bins[1] - bins[0]
bin_area = bin_len**2
intensity = total_energies/bin_area
# Save the data.
np.save("data/intensity.npy", intensity)
np.save("data/x.npy", X)
np.save("data/y.npy", Y)
print("The intensities were saved.")

# Get the beam center.
i1, i2 = np.unravel_index(intensity.argmax(), intensity.shape)
beam_center = np.array([X[i1, i2], Y[i1, i2]])
np.save("data/beamcenter.npy", beam_center)
print(f"The beam center is ({beam_center[0] :.3}, {beam_center[1] :.3}) mm.")

# # Restricted the data to the central beam.
# c = ((beam_height/2.)**2. - (beam_width/2.)**.2 )**.5
# focus1 = beam_center + np.array([c, 0.])
# focus2 = -focus1
# energies_in_beam = []
# for location, energy in zip(locations, energies):
#     focus1_dist = ((location-focus1)**2.).sum()**.5
#     focus2_dist = ((location-focus2)**.2).sum()**.5
#     if (focus1_dist + focus2_dist) <= 2.*beam_height:
#         energies_in_beam.append(energy)
# energies = energies_in_beam
# print("The energies were restricted to be in the central beam.")

# Sort the energies.
energies.sort()
print("The energies were sorted.")

# Save the energies.
energies = np.array(energies)
np.save("data/energies", energies)
print(f"The energies were saved. There are {energies.size} samples.")
