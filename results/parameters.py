import math as ma


# Independent parameters.
K = 3. # wiggler constant
num_electrons = 10**7
num_periods = 50
electron_energy_GeV = 2.4 #GeV
electron_energy_J = electron_energy_GeV * (1e9*1.602176634e-19) #Joules
max_magnetic_field = 1.6 #T
dist_to_detector = 20e3 #mm
detector_radius = 50. #mm
current = 1. #amp

# Compute the Lorentz factor.
speed_of_light = 299792458. #m/s
electron_mass = 9.1093837015e-31 #kg
electron_speed = (speed_of_light
                  *ma.sqrt(1.-((electron_mass*speed_of_light**2.)
                               /electron_energy_J)**2.))
lorentz_factor = 1./ma.sqrt(1.-(electron_speed/speed_of_light)**2.)

# Get the beam spread at the detector.
horizontal_opening_angle = 1./lorentz_factor #rad
vertical_opening_angle = K/lorentz_factor #rad
beam_width = 2.*dist_to_detector*ma.tan(horizontal_opening_angle/2.) #mm
beam_height = 2.*dist_to_detector*ma.tan(vertical_opening_angle/2.) #mm
beam_cross_section = ma.pi * beam_width/2. * beam_height/2. ##mm^2

# Print some parameters.
print("The predicted opening angles are "
      + f"{horizontal_opening_angle*1000. :.3} mrad and "
      + f"{vertical_opening_angle*1000. :.3} mrad.")
