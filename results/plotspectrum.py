import csv
import math as ma
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import statistics as stat

from parameters import *


# Get the data.
energies = np.load("data/energies.npy")

# Bin the data.
num_bins = 50
bins = np.linspace(min(energies), max(energies), num_bins+1)
bin_width = bins[1] - bins[0]
midpoints = bins + bin_width/2.0
midpoints = midpoints[:-1]
indices = np.digitize(energies, bins)
total_energies = np.zeros(num_bins)
for idx, sample in zip(indices, energies):
    if not (idx <= 0 or idx > num_bins):
        total_energies[idx-1] += sample
# Weigh by the current.
total_energies *= (current*(1./1.602176634e-19)/num_electrons)

# Plot the spectrum.
plt.plot(midpoints, total_energies)
plt.xlabel("photon energy (keV)")
plt.ylabel("deposited-energy density per second (1/s)")
plt.xlim(0.)
plt.ylim(0.)
plt.show()
