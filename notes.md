# To-Do List for the Undulator

## To-Do List

* Why are so few counts being picked up for the brilliance?
* Restrict the energies to those in the central beam.
* Speed up ```plotspectrum.py``` by using the information that the energies are sorted.

* The spectrum (and hence the brilliance) seems to wrong.
  - Maybe not directly dealing with K would help. I don't need it in any of the formulas.

* Use start_energy and so on. (Don't unit *initial*.) (Actually, I might not need this at all if I just use electron_energy.)



## General Notes

*


## Wiggler Understanding


## Geant4 Understanding

* Look more into how to use the modular-physics list with specific physics.
* Why are there so many step to make a chord finder?
