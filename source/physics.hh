#ifndef PHYSICS_HH
#define PHYSICS_HH 0

#include "G4VModularPhysicsList.hh"
#include "G4EmLivermorePhysics.hh"
#include "G4SynchrotronRadiation.hh"

#include "G4VPhysicsConstructor.hh"
#include "G4Gamma.hh"
#include "G4Electron.hh"


class MySynchrotronPhysics : public G4VPhysicsConstructor {
public:
  MySynchrotronPhysics() {};
  virtual void ConstructParticle() {
    G4Gamma::Definition();
  }
  virtual void ConstructProcess() {
    G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
    ph->RegisterProcess(new G4SynchrotronRadiation(),
                        G4Electron::Definition());
  }
};


class MyPhysicsList : public G4VModularPhysicsList {
public:
  MyPhysicsList() {
    RegisterPhysics(new G4EmLivermorePhysics());
    RegisterPhysics(new MySynchrotronPhysics());
  }
};


#endif //PHYSICS_HH
