#ifndef MAGFIELD_HH
#define MAGFIELD_HH 0

#include <cmath>

#include "G4MagneticField.hh"
// #include "G4UniformMagField.hh"

#include "G4SystemOfUnits.hh"

#include "parameters.hh"


class MyField : public G4MagneticField {
public:
  MyField() {};
  virtual void GetFieldValue(const double[4], double*) const;
};


void MyField::GetFieldValue(const double Point[4], double *field) const {
  double x = Point[0];
  x += undulatorLen/2.0; //so that the start is at 0.
  field[0] = 0.0*tesla;
  field[1] = 0.0*tesla;
  field[2] = maxMagFieldStr * cos(x*(2.0*M_PI/spatialPeriod));
  // Maybe I do need to account for the energy loss.

}

#endif //MAGFIELD_HH
