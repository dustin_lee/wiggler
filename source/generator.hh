#ifndef GENERATOR_HH
#define GENERATOR_HH 0

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "G4Electron.hh"
#include "G4ThreeVector.hh"
#include "G4SystemOfUnits.hh"

#include "parameters.hh"


class MyGenerator : public G4VUserPrimaryGeneratorAction {
public:
  MyGenerator() {
    particleGun = new G4ParticleGun(1);
  }
  ~MyGenerator() {
    delete particleGun;
  }

  G4ParticleGun* particleGun;
private:
  virtual void GeneratePrimaries(G4Event*);
};


void MyGenerator::GeneratePrimaries(G4Event* event) {
  particleGun->SetParticleDefinition(G4Electron::Definition());
  particleGun->SetParticleEnergy(initElectronEnergy);
  particleGun->SetParticlePosition(G4ThreeVector(-totalLen/2.0 + 0.1*cm,
                                                 0.0*cm, 0.0*cm));
  particleGun->SetParticleMomentumDirection(G4ThreeVector(1.0, 0.0, 0.0));
  // particleGun->SetParticleTime(0.0);
  particleGun->GeneratePrimaryVertex(event);
}


#endif //GENERATOR_HH
