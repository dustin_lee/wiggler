#ifndef CONSTRUCTION_HH
#define CONSTRUCTION_HH 0

#include <string>

#include "G4VUserDetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"

#include "G4FieldManager.hh"

#include "G4HelixSimpleRunge.hh"
#include "G4MagIntegratorDriver.hh"
#include "G4ChordFinder.hh"
#include "G4Mag_UsualEqRhs.hh"

#include "G4SystemOfUnits.hh"

#include "parameters.hh"
#include "detector.hh"
#include "magfield.hh"


class MyConstruction : public G4VUserDetectorConstruction {
public:
  MyConstruction() {};
  virtual G4VPhysicalVolume* Construct();
};


G4VPhysicalVolume* MyConstruction::Construct() {
  auto* nist = G4NistManager::Instance(); //for materials

  //Construct the environment.
  auto* worldLogical = new
    G4LogicalVolume(new G4Box("worldSolid", totalLen/2.0, 10.0*cm, 10.0*cm),
                    nist->FindOrBuildMaterial("G4_Galactic"),
                    "worldLogical");
  auto* worldPhysical = new
    G4PVPlacement(NULL, G4ThreeVector(0.0, 0.0, 0.0),
                  worldLogical, "WorldPhysical", NULL,
                  false, 0, true);

  // Make the undulator.
  auto* undulatorLogical = new
    G4LogicalVolume(new G4Box("undulatortSolid",
                              undulatorLen/2.0, 1.0*cm, 1.0*cm),
                    nist->FindOrBuildMaterial("G4_Galactic"),
                    "undulatorLogical");
  auto location = G4ThreeVector(undulatorCenterX, 0.0*cm, 0.0*cm);
  new G4PVPlacement(NULL, location,
                    undulatorLogical, "undulatorPhysical", worldLogical,
                    false, 0, true);
  // Add the magnetic field.
  auto* fieldMgr = new G4FieldManager();
  undulatorLogical->SetFieldManager(fieldMgr, true);
  auto* magField = new MyField();
  fieldMgr->SetDetectorField(magField);
  // Add the chord finder.
  fieldMgr->CreateChordFinder(magField);
  auto* fieldEquation = new G4Mag_UsualEqRhs(magField);
  auto* fieldStepper = new G4HelixSimpleRunge(fieldEquation);
  auto* pIntgrDriver = new G4MagInt_Driver(.01*mm, fieldStepper,
                                        fieldStepper->GetNumberOfVariables());
  auto* chordFinder = new G4ChordFinder(pIntgrDriver);
  fieldMgr->SetChordFinder(chordFinder);

  // Make the detector.
  auto* detectorLogical = new
    G4LogicalVolume(new G4Tubs("detectorSolid",
                    0.0*cm, 5.0*cm,       //Inner and outer radii.
                    0.001*cm,             //Thickness.
                    0.0*deg, 360.0*deg),  //Angles of rotation.
                    nist->FindOrBuildMaterial("G4_Galactic"),
                    "detectorLogical");
  auto* detectorRot = new G4RotationMatrix();
  detectorRot->rotateY(90.0*deg);
  new G4PVPlacement(detectorRot, G4ThreeVector(detectorLocX, 0.0*cm, 0.0*cm),
                    detectorLogical, "detectorPhysical", worldLogical,
                    false, 0, true);
  auto* detector = new Detector("detector");
  detectorLogical->SetSensitiveDetector(detector);

  return worldPhysical;
}


#endif //CONSTRUCTION_HH
