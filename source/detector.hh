#ifndef DETECTOR_HPP
#define DETECTOR_HPP 0

#include <fstream>

#include "G4VSensitiveDetector.hh"


class Detector : public G4VSensitiveDetector {
public:
  Detector(G4String name) : G4VSensitiveDetector(name) {}
  virtual G4bool ProcessHits(G4Step*, G4TouchableHistory*);
};


G4bool Detector::ProcessHits(G4Step* step, G4TouchableHistory* hist) {
  G4Track* track = step->GetTrack();
  track->SetTrackStatus(fStopAndKill);

  auto* stepPoint = step->GetPreStepPoint();
  auto particleName = track->GetParticleDefinition()->GetParticleName();
  if (particleName == "gamma") {
    auto dataFile = std::ofstream("../results/data/data.csv",
                                  std::ios_base::app);
    dataFile << stepPoint->GetKineticEnergy()/keV << ','
             << stepPoint->GetPosition().getY()/mm << ','
             << stepPoint->GetPosition().getZ()/mm << '\n';
    dataFile.close();
  }

  return true;
}

#endif //DETECTOR_HPP
