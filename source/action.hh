#ifndef ACTION_HH
#define ACTION_HH 0

#include "G4VUserActionInitialization.hh"

#include "generator.hh"


//Change the name to initialActions?
class InitialActions : public G4VUserActionInitialization {
public:
  InitialActions() {}
  virtual void Build() const;
};


void InitialActions::Build() const {
  auto* generator = new MyGenerator();
  SetUserAction(generator); //This function is from the base class.
}


#endif //ACTION_HH
