#ifndef PARAMETERS_HH
#define PARAMETERS_HH 0

#include <cmath>
#include <iostream>

#include "G4ThreeVector.hh"
#include "G4SystemOfUnits.hh"


// Independent parameters.
double K = 3.0; //wiggler constant.
// To be a wiggler, the wiggler constant must be bigger than 1.
int numPeriods = 50;
double maxMagFieldStr = 1.6*tesla;
double initElectronEnergy = 2.4*GeV;
double distToDetector = 20.0*m; //from the center of the undulator

// Dependent parameters.
double totalLen;
double spatialPeriod;
double electronAmplitude;
double undulatorLen;
double undulatorStartX;
double undulatorCenterX;
double detectorLocX;


void makeParameters() {
  // Compute the spatial period.
  spatialPeriod = K/(0.934*(maxMagFieldStr/tesla))*cm;
  std::cout << "The spatial period is " << spatialPeriod/cm << " cm.\n";
  // Compute the undulator dimensions.
  undulatorLen = numPeriods*spatialPeriod;
  totalLen = 1.0*cm + undulatorLen/2.0 + distToDetector + 1.0*cm;
  double xEnd = totalLen/2.0;
  double xStart = -xEnd;
  detectorLocX = xEnd - 1.0*cm;
  undulatorStartX = xStart + 1.0*cm;
  undulatorCenterX = undulatorStartX + undulatorLen/2.0;
}


#endif //PARAMETERS_HH
